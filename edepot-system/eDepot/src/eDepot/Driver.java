package eDepot;

public class Driver {

	private String username;
	private String password;
	private boolean isManager;
	private boolean isAvailable;
	
	public Driver (String username, String password, boolean isManager, boolean isAvailable){
		this.username = username;
		this.password = password;
		this.isManager = isManager;
		this.isAvailable = isAvailable;
	}
	
	//Tests to see if the user entered password matches that stored on the system.
	//Returns a boolean.
	public boolean validatePassword(String userPassword) {
		if (password.equals(userPassword)){
			return true;
		}
		else{
			return false;
		}
	}
	
	public boolean getIsManager(){
		return isManager;
	}
	
	public String getUsername(){
		return username;
	}
	
	public String getPassword(){
		return password;
	}
	
	public boolean isAvailable(){
		return isAvailable();
	}
}

