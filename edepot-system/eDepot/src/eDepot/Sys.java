package eDepot;

import java.time.*;

public class Sys {
	
	private boolean userAuthenticate;
	private Screen screen;
	private Keypad keypad;
	private SystemDatabase database;
	private String username;
	private boolean currentUserManager;
	private boolean vehicleIsAvailable;
	
	public Sys(){
		userAuthenticate = false;
		username = "Not yet set";
		screen = new Screen();
		keypad = new Keypad();
		database = new SystemDatabase();
		currentUserManager = false;
		vehicleIsAvailable = true;
	}
	

    public void run() {
    	
        while (true) {
            while (!userAuthenticate) {
                screen.displayNewLineMessage("");
                screen.displayNewLineMessage("Welcome!");
                authenticateUser();
                
            }
			
            performTask();
			userAuthenticate = false;
			username = "Not yet set!";
			
			screen.displayNewLineMessage("");
			screen.displayNewLineMessage("Thank you for using the eDepot Management System!");
		}
	}
    
	private void authenticateUser(){
		screen.displayNewLineMessage("");
		screen.displayNewLineMessage("Please enter your username: ");
		String userUsername = keypad.getTextInput();
		
		screen.displayNewLineMessage("");
		screen.displayNewLineMessage("Please enter your password: ");
		String userPassword = keypad.getTextInput();
		
		userAuthenticate = database.authenticateUser(userUsername, userPassword);
		
		currentUserManager = database.getManagerStatus(userUsername);
		
		if (userAuthenticate){
			username = userUsername;
		}
		else{
			System.out.println("Wrong username and/or password. Please try again.");
		}
	}
	
	private void logOff(){
		
		currentUserManager = false;
		userAuthenticate = false;
		
		new Sys().run();
	}
	
	private void performTask(){
		
			
		int menuSelection = displayMainMenu();
	
		switch (menuSelection){
		
		case 1 : database.displaySchedule();
		performTask();
		break;
		
		case 2 : createSchedule();
		break;
		
		case 3 : reassignVehicle();
		break;
		
		case 4 : newUserAdd();
		break;
		
		case 5 : newVehicleAdd();
		break;
		
		case 6 : logOff();
		break;
		
		case 7 : terminateProgram();
		break;
		
		default:
            screen.displayNewLineMessage("You did not enter a valid selection. Please try again!");
           performTask();
            break;
		}
	}

    
    public int displayMainMenu(){
    	
    	screen.displayNewLineMessage("");
    	
    	if(currentUserManager == true){
    		screen.displayNewLineMessage("Currently logged on as Depot Manager!!");
    	}else{
    		screen.displayNewLineMessage("Currently logged on as Driver!!");
    	}
    	
        screen.displayNewLineMessage("");
        screen.displayNewLineMessage("Main Menu :");
        screen.displayNewLineMessage("Option 1: View Schedules");
        screen.displayNewLineMessage("Option 2: Setup New Schedule/Delete Existing Schedule (Depot Manager only)");
        screen.displayNewLineMessage("Option 3: Reassign Vehicle to Another Depot (Depot Manager only)");
        screen.displayNewLineMessage("Option 4: Add a new Driver or Manager to the system (Depot Manager only)");
        screen.displayNewLineMessage("Option 5: Add a new Vehicle to the system (Depot Manager only)");
        screen.displayNewLineMessage("Option 6: Sign Out");
        screen.displayNewLineMessage("Option 7: Exit Program");
        screen.displayNewLineMessage("");
        screen.displayMessage("Enter Choice : ");

        return keypad.getNumberInput();
    }
    
    public void newUserAdd(){
    	
    	if (currentUserManager == true){
    		
    		screen.displayNewLineMessage("");
    		screen.displayNewLineMessage("Accessed as Depot Manager.");
    		
    		screen.displayNewLineMessage("");
    		screen.displayNewLineMessage("Please enter the username of the new driver/manager: ");
    		keypad.getTextInput();
    		String userUsername = keypad.getTextInput();
    		
    		screen.displayNewLineMessage("");
    		screen.displayNewLineMessage("Please enter the new password for the account: ");
    		String userPassword = keypad.getTextInput();
    		
    		screen.displayNewLineMessage("");
    		screen.displayNewLineMessage("Please enter the Depot Manager status of the new user ('true' to set as manager/'false' to set as driver): ");
    		String userIsManagerStr = keypad.getTextInput();
    		
    		boolean userIsManager = Boolean.valueOf(userIsManagerStr);
    		
    		boolean isAvailable = true;
    		
    		database.addDriver(userUsername, userPassword, userIsManager, isAvailable);
    		
    		screen.displayNewLineMessage("");
    		screen.displayNewLineMessage("User successfully added.");
    		
    		database.listDrivers();
    		
    		screen.displayNewLineMessage("");
    		screen.displayNewLineMessage("Returning to main menu...");
    		performTask();
    		
    	}else{
    		
    		screen.displayNewLineMessage("");
    		screen.displayNewLineMessage("You do not have permission to access this function.");
    		
    		performTask();
    		
    	}
    	
    }
    
    public void newVehicleAdd(){
    	
    	if(currentUserManager == true){
    		
    		screen.displayNewLineMessage("");
    		screen.displayNewLineMessage("Accessed as Depot Manager.");
    		
    		screen.displayNewLineMessage("");
    		screen.displayNewLineMessage("Please enter the make of the new vehicle: ");
    		keypad.getTextInput();
    		String userMake = keypad.getTextInput();
    		
    		screen.displayNewLineMessage("");
    		screen.displayNewLineMessage("Please enter the model of the new vehicle: ");
    		String userModel = keypad.getTextInput();
    		
    		screen.displayNewLineMessage("");
    		screen.displayNewLineMessage("Please enter the weight of the new vehicle: ");
    		int userWeight = keypad.getNumberInput();
    		
    		screen.displayNewLineMessage("");
    		screen.displayNewLineMessage("Please enter the Registration Number of the new vehicle: ");
    		keypad.getTextInput();
    		String userRegNo = keypad.getTextInput();
    		
    		screen.displayNewLineMessage("");
    		screen.displayNewLineMessage("Please enter the home depot of the new vehicle: ");
    		String userDepot = keypad.getTextInput();
    		
    		boolean isAvailable = true;
    		
    		database.addVehicle(userMake, userModel, userWeight, userRegNo, isAvailable, userDepot);
    		
    		screen.displayNewLineMessage("");
    		screen.displayNewLineMessage("Vehicle successfully added.");
    		
    		database.listVehicles();
    		
    		screen.displayNewLineMessage("");
    		screen.displayNewLineMessage("Returning to main menu...");
    		performTask();
    		
    	}else{
    		
    		screen.displayNewLineMessage("");
    		screen.displayNewLineMessage("You do not have permission to access this function.");
    		
    		performTask();
    		
    	}
    	
    }
    
    public void createSchedule(){
    	
    	if(currentUserManager == true){
    		
    		LocalDate currentDate = LocalDate.now();
    		LocalTime currentTime = LocalTime.now();
    		
    		screen.displayNewLineMessage("");
    		screen.displayNewLineMessage("Accessed as Depot Manager.");
    		
    		screen.displayNewLineMessage("");
    		screen.displayNewLineMessage("Please enter the username of the driver you wish to create a schedule for: ");
    		keypad.getTextInput();
    		String userDriverSelection = keypad.getTextInput();
    		
    		screen.displayNewLineMessage("");
    		screen.displayNewLineMessage("Please enter the reg number of the vehicle you wish to assign to the schedule: ");
    		String userVehicleSelection = keypad.getTextInput();
    		
    		screen.displayNewLineMessage("");
    		screen.displayNewLineMessage("Please enter the name of the client: ");
    		String userClientSelection = keypad.getTextInput();
    		
    		screen.displayNewLineMessage("");
    		screen.displayNewLineMessage("Please enter the date on which the job begins (yyyy-mm-dd): ");
    		String userStartDateSelectionStr = keypad.getTextInput();
    		
    		LocalDate userStartDateSelection = LocalDate.parse(userStartDateSelectionStr);
    		
    		screen.displayNewLineMessage("");
    		screen.displayNewLineMessage("Please enter the time at which the job begins (hh:mm:ss): ");
    		String userStartTimeSelectionStr = keypad.getTextInput();
    		
    		LocalTime userStartTimeSelection = LocalTime.parse(userStartTimeSelectionStr);
    		
    		screen.displayNewLineMessage("");
    		screen.displayNewLineMessage("Please enter the date on which the job ends (yyyy-mm-dd): ");
    		String userEndDateSelectionStr = keypad.getTextInput();
    		
    		LocalDate userEndDateSelection = LocalDate.parse(userEndDateSelectionStr);
    		
    		screen.displayNewLineMessage("");
    		screen.displayNewLineMessage("Please enter the time at which the job ends (hh:mm:ss): ");
    		String userEndTimeSelectionStr = keypad.getTextInput();
    		
    		LocalTime userEndTimeSelection = LocalTime.parse(userEndTimeSelectionStr);
    		
    		String beginState = "pending";
    		
    		Period duration = Period.between(userStartDateSelection, userEndDateSelection);
    		
    		database.createSchedule(userDriverSelection, userVehicleSelection, userClientSelection, userStartDateSelection, userStartTimeSelection,
    				userEndDateSelection, userEndTimeSelection, beginState, duration);
    		
    		screen.displayNewLineMessage("");
    		screen.displayNewLineMessage("Created successfully.");
    		performTask();
    		
    	}else{
    		
    		screen.displayNewLineMessage("");
    		screen.displayNewLineMessage("You do not have permission to access this function.");
    		
    		userAuthenticate = true;
    		performTask();
    		
    	}
    	
    }
      
    public void reassignVehicle() {
    	
    
    if(currentUserManager == true){
    	
    	database.listVehicles();
    	
    	screen.displayNewLineMessage("");
		screen.displayNewLineMessage("Accessed as Depot Manager.");
		
		screen.displayNewLineMessage("");
		screen.displayNewLineMessage("Please enter the registration number of the vehicle that you would like to reassign: ");
		keypad.getTextInput();
		String userUserRegNo = keypad.getTextInput();
		
		screen.displayNewLineMessage("");
		screen.displayNewLineMessage("Please enter the current home depot of the vehicle: ");
		String userCurrentDepot = keypad.getTextInput();
    	
		screen.displayNewLineMessage("");
		screen.displayNewLineMessage("Please enter the new home depot of the vehicle: ");
		String userNewDepot = keypad.getTextInput();

		
		database.reassignVehicle(userUserRegNo, userCurrentDepot, userNewDepot);
		
		/*database.newDepots();*/
		
		screen.displayNewLineMessage("Vehicle with the registration " + userUserRegNo + ", has been reassigned from " + userCurrentDepot + " to " + userNewDepot);
		
		screen.displayNewLineMessage("");
		screen.displayNewLineMessage("Returning to main menu...");
		performTask();
    	
    }else{
		
		screen.displayNewLineMessage("");
		screen.displayNewLineMessage("You do not have permission to access this function.");
		
		performTask();
		
	}
    
    }
	//This method call terminates the program for the user.
	public static void terminateProgram(){
		
		System.out.println("Program terminated.");
		
		System.exit(0);
		return;
	}
	
}
