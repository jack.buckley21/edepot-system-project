package eDepot;

//This class is designed to output all messages
//from the machine to the user.
public class Screen {
	
	public Screen(){

	}
	
	public void displayMessage(String output){
		System.out.print(output);
	}
	
	public void displayNewLineMessage(String output){
		System.out.println(output);
	}

}