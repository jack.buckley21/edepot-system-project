package eDepot;

import java.util.Scanner;
import java.time.*;

//Used in the system to help emulate user input
public class Keypad {
	private Scanner userInput;
	
	public Keypad(){
		userInput = new Scanner(System.in);
	}
	
	public String getTextInput(){
		return userInput.nextLine();
	}
	
	public int getNumberInput(){
		return userInput.nextInt();
	}
	
}