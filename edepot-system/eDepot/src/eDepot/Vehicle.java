package eDepot;

public class Vehicle {

	private String make;
	private String model;
	private int weight;
	private String regNo;
	private boolean isAvailable;
	private String depot;
	
	public Vehicle(String make, String model, int weight, String regNo, boolean isAvailable, String depot){
		this.make = make;
		this.model = model;
		this.weight = weight;
		this.regNo = regNo;
		this.isAvailable = isAvailable;
		this.depot = depot;
	}
	
	public String getMake(){
		return make;
	}
	
	public String getModel(){
		return model;
	}
	
	public String getRegNo(){
		return regNo;
	}
	
	public String getDepot(){
		return depot;
	}
	
	public boolean checkAvailable(boolean isAvailable){
		return isAvailable;
	}
	
}
