package eDepot;

//This class is used to initialise the System by calling the 'Run'
//object in the Sys class.
public class Start {
	
	public static void main(String[] args) {
		
		new Sys().run();
	}

}
