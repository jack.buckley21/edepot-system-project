package eDepot;

import java.util.*;
import java.time.*;

public class SystemDatabase {

	private Depot depot1;
	private Depot depot2;
	ArrayList<Driver> arrayDrivers = new ArrayList<Driver>();
	ArrayList<Vehicle>arrayVehicles = new ArrayList<Vehicle>();
	ArrayList<WorkSchedule> arraySchedules = new ArrayList<WorkSchedule>();

	public SystemDatabase() {

		depot1 = new Depot("depot1");
		depot2 = new Depot("depot2");

		arrayDrivers.add(new Driver("andy123", "password123", true, true));
		arrayDrivers.add(new Driver("james456", "password456", false, true));
		arrayDrivers.add(new Driver("bob765", "abc123", false, true));
		arrayDrivers.add(new Driver("ian098", "def123", false, true));
		arrayDrivers.add(new Driver("glyn789", "password1", true, true));

		arrayVehicles.add(new Vehicle("Scania", "a123", 14000, "ab12 cde", true, "depot1"));
		arrayVehicles.add(new Vehicle("Renault", "z987", 16500, "xy12 fgh", true, "depot1"));
		arrayVehicles.add(new Vehicle("Volvo", "FH16", 19000, "mt06 lne", true, "depot1"));
		arrayVehicles.add(new Vehicle("MAN", "TGX", 18300, "hy13 lvf", true, "depot1"));
		arrayVehicles.add(new Vehicle("Tesla", "Semi", 15600, "te s1a", true, "depot1"));

	}

	private Driver getDriver(String username) {
		for (Driver currentDriver : arrayDrivers) {
			if (currentDriver.getUsername().equals(username)) {
				return currentDriver;
			}
		}

		return null;
	}

	public boolean getManagerStatus(String userUsername) {
		for (Driver currentDriver : arrayDrivers) {

			if (currentDriver.getUsername().toString().contains(userUsername)) {
				return currentDriver.getIsManager();
			}
		}
		return false;
	}

	public boolean authenticateUser(String userUsername, String userPassword) {
		Driver userDriver = getDriver(userUsername);

		if (userDriver.equals("null")) {
			return false;
		} else {
			return userDriver.validatePassword(userPassword);
		}
	}

	public void addDriver(String userUsername, String userPassword, boolean userIsManager, boolean isAvailable) {

		arrayDrivers.add(new Driver(userUsername, userPassword, userIsManager, isAvailable));

	}

	public void listDrivers() {

		System.out.println("");
		System.out.println("The list of drivers' usernames now present on the system is:");
		System.out.println("");

		for (Driver username : arrayDrivers) {
			System.out.println("Username of Driver: " + username.getUsername() + ", Depot Manager?: " + username.getIsManager());
		}
	}

	public void addVehicle(String userMake, String userModel, int userWeight, String userRegNo, boolean isAvailable,
			String userDepot) {

		arrayVehicles.add(new Vehicle(userMake, userModel, userWeight, userRegNo, isAvailable, userDepot));

	}

	public void listVehicles() {

		System.out.println("");
		System.out.println("The list of registration numbers for vehicles now present on the system is:");
		System.out.println("");

		
		for (Vehicle userVehicle : arrayVehicles){
			//System.out.println(regNo.getRegNo());
			System.out.println("Vehicle Make: " + userVehicle.getMake() + ", Vehicle Model: " + userVehicle.getModel() + ", Registration Number: " + userVehicle.getRegNo() + ", Depot: " + userVehicle.getDepot() + ".");

			}
		}
	
	public void createSchedule(String userDriver, String userVehicle, String userClient, LocalDate userStartDate, LocalTime userStartTime, LocalDate userEndDate, LocalTime userEndTime, String beginState, Period duration){
		
		arraySchedules.add(new WorkSchedule(userDriver, userVehicle, userClient, userStartDate, userStartTime, userEndDate, userEndTime, beginState, duration));
		
	}
	
	public void displaySchedule() {
		
		for(WorkSchedule userDriver : arraySchedules){
			System.out.println("Driver: " + userDriver.getAssignedDriver() + ", Vehicle(Reg Number): " + userDriver.getAssignedVehicle() + ", Start Date: " + userDriver.getStartDate() + ", Duration: " + userDriver.getDuration().getDays() + "days.");
		}
	}


	

	public void reassignVehicle(String userUserRegNo, String userCurrentDepot, String userNewDepot) {

		for (Vehicle RegNo : arrayVehicles) {

			//if (RegNo.getDepot().toString().contains(userCurrentDepot)) {
			//	arrayVehicles.set(new Vehicle(5, "userNewDepot"));
			//}
		}
	}
	
	public void newDepots() {

		System.out.println("");
		System.out.println("The list of new locations for vehicles:");
		System.out.println("");

		for (Vehicle depot : arrayVehicles) {
			System.out.println(depot.getDepot());

		}
	}
	
}