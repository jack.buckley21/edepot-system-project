package eDepot;

import java.time.*;

public class WorkSchedule {
	
	private String assignedDriver;
	private String assignedVehicle;
	private String clientName;
	private LocalDate startDate;
	private LocalTime startTime;
	private LocalDate endDate;
	private LocalTime endTime;
	private String state;
	private Period duration;
	
	public WorkSchedule(String assignedDriver, String assignedVehicle, String clientName, LocalDate startDate,
			LocalTime startTime, LocalDate endDate, LocalTime endTime,String state, Period duration){
		
		this.assignedDriver = assignedDriver;
		this.assignedVehicle = assignedVehicle;
		this.clientName = clientName;
		this.startDate = startDate;
		this.startTime = startTime;
		this.endDate = endDate;
		this.endTime = endTime;
		this.state = state;
		this.duration = duration;
			
	}
	
	public String getAssignedDriver(){
		return assignedDriver;
	}
	
	public String getAssignedVehicle(){
		return assignedVehicle;
	}

	public String getClientName(){
		return clientName;
	}
	
	public LocalDate getStartDate(){
		return startDate;
	}
	
	public LocalTime getStartTime(){
		return startTime;
	}
	
	public LocalDate getEndDate(){
		return endDate;
	}
	
	public LocalTime getEndTime(){
		return endTime;
	}
	
	public String getState(){
		return state;
	}
	
	public Period getDuration(){
		return duration;
	}
	
}
